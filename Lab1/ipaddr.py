#!/usr/bin/python
# =============================================================================
#        File : ipaddr.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#      Author : Drew Gislsason
#        Date : 3/1/2017
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#

from sense_hat import SenseHat
import time

sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

# give time to get on the Wi-Fi network
import time
time.sleep(10)

# get our local IP address. Requires internet access (to talk to gmail.com).
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
addr = s.getsockname()[0]
s.close()

# display that address on the sense had
sense.show_message(str(addr))

import time

sense = SenseHat()

# set colors
y = [255, 255, 0]
v = [159, 0, 255]
b = [0, 0, 255]
n = [0, 0, 0]

# go dawgs message
sense.show_message("GO DAWGS!", scroll_speed=0.1, text_colour=y, back_colour=v)
# set image
image = [y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v,
        y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v,
        y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v,
        y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v]

sense.set_pixels(image)

# rotating U and W
angles = [0, 90, 180, 270]
sense.show_letter("U", text_colour=v, back_colour=y)
for r in angles:
        sense.set_rotation(r)
        time.sleep(0.5)

sense.show_letter("W", text_colour=y, back_colour=v)
for r in angles:
        sense.set_rotation(r)
        time.sleep(0.5)

# take environment readings
t = round(sense.get_temperature(), 1)
p = round(sense.get_pressure(), 1)
h = round(sense.get_humidity(), 1)

msg = "Temp = %s, Press=%s, Hum=%s" % (t,p,h)
sense.set_rotation(0)
sense.show_message(msg, text_colour=b, back_colour=n)


