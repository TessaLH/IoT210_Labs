#!/usr/bin/python
from sense_hat import SenseHat
import time

sense = SenseHat()

# set colors
y = [255, 255, 0]
v = [159, 0, 255]
b = [0, 0, 255]
n = [0, 0, 0]

# go dawgs message
sense.show_message("GO DAWGS!", scroll_speed=0.1, text_colour=y, back_colour=v)
# set image
image = [y,y,y,y,y,y,y,y,
	v,v,v,v,v,v,v,v,
	y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v,
	y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v,
	y,y,y,y,y,y,y,y,
        v,v,v,v,v,v,v,v]

sense.set_pixels(image)

# rotating U and W
angles = [0, 90, 180, 270]
sense.show_letter("U", text_colour=v, back_colour=y)
for r in angles:
	sense.set_rotation(r)
	time.sleep(0.5)

sense.show_letter("W", text_colour=y, back_colour=v)
for r in angles:
        sense.set_rotation(r)
        time.sleep(0.5)

# take environment readings
t = round(sense.get_temperature(), 1)
p = round(sense.get_pressure(), 1)
h = round(sense.get_humidity(), 1)

msg = "Temp = %s, Press=%s, Hum=%s" % (t,p,h)
sense.set_rotation(0)
sense.show_message(msg, text_colour=b, back_colour=n)

# snake: tilt gyro and pixel will travel that direction
nw_x = 3
nw_y = 3
sense.set_pixel(nw_x, nw_y, b)

while True:
	x, y, z = sense.get_accelerometer_raw().values()
	x = round(x, 0)
	y = round(y, 0)

	if x == -1 and nw_y > 0:
		#sense.set_rotation(180)
		nw_y -= 1
	elif x == 1 and nw_y < 7:
		nw_y += 1
	if y == 1 and nw_x < 7:
		#sense.set_rotation(90)
		nw_x += 1
	elif y == -1 and nw_x > 0:
		#sense.set_rotation(270)
		nw_x -= 1

	sense.set_pixel(nw_x, nw_y, b)
	time.sleep(0.5)
