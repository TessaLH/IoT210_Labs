#!/usr/bin/python
# =============================================================================
#        File : http_server.py
# Description : HTTP server echos the verb and input
#      Author : Drew Gislsason
#        Date : 4/6/2017

#     Edit by : Tessa Lombard-Henley
#        Date : 1/15/2018
#     Changes : Add grocery list API
# =============================================================================
import random
import string
import json
import sys

PORT = 5000

from flask import Flask, request

#list = {}
list = "\nGROCERY LIST:\n"
fruitList = "\nFRUIT:\n"
veggieList = "\nVEGETABLES:\n"
# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

@app.route("/my/api", methods=['GET', 'HEAD' 'PUT', 'POST', 'DELETE'])
def ApiV1Echo():

  rsp_data = "Echo:\nMethod: "

  if request.method == 'GET':
    rsp_data += "GET"
  elif request.method == 'HEAD':
    rsp_data += "HEAD"
  elif request.method == 'PUT':
    rsp_data += "PUT"
  elif request.method == 'POST':
    rsp_data += "POST"
  elif request.method == 'DELETE':
    rsp_data += "DELETE"

  rsp_data += "\nArgs: "
  for arg in request.args:
    rsp_data += str(arg) + ":" + request.args[arg] + " "

  rsp_header = "Echo: \nHeaders: "
  for h_key, h_val in request.headers:
    rsp_header += "Key: " + str(h_key) + "=" + str(h_val)

  rsp_data += "\nData:\n"
  rsp_data += request.get_data()
  rsp_data += "\n"
  rsp_data += "Headers:\n"
  rsp_data += rsp_header

  return rsp_data, 200


# ================ API for creating grocery list ================ #
@app.route("/list", methods = ['GET', 'POST', 'PUT', 'DELETE'])
def list_resp():
  global list
  if request.method == 'POST':
    list += str(request.data) + "\n"
  elif request.method == 'GET':
    return  list + fruitList + veggieList
  elif request.method == 'DELETE':
    list = "\nGROCERY LIST:\n"
    fruitList = "\nFRUIT:\n"
    veggieList = "\nVEGETABLES:\n"

  for arg in request.args:
    list += str(arg)+ + ":" + request.args[arg] + "\n"

  return list + fruitList + veggieList, 200

# ============ FRUIT ===========================================#
@app.route("/list/fruit", methods = ['GET', 'POST', 'PUT', 'DELETE'])
def fruit_list_resp():
  global fruitList
  if request.method == 'POST':
    fruitList += str(request.data) + "\n"
  elif request.method == 'GET':
    return  fruitList
  elif request.method == 'DELETE':
    fruitList = "\nFRUIT:\n"

  for arg in request.args:
    fruitList += str(arg)+ + ":" + request.args[arg] + "\n"

  return fruitList, 200

# ============ VEGGIES =================================#
@app.route("/list/vegetables", methods = ['GET', 'POST', 'PUT', 'DELETE'])
def veg_list_resp():
  global veggieList
  if request.method == 'POST':
    veggieList += str(request.data) + "\n"
  elif request.method == 'GET':
    return  veggieList
  elif request.method == 'DELETE':
    veggielist = "\nVEGETABLES:\n"

  for arg in request.args:
    veggieList += str(arg)+ + ":" + request.args[arg] + "\n"

  return veggieList, 200


# ============================== Main ====================================

if __name__ == "__main__":

  print "HTTP Server"

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)


