#!/usr/bin/python

import httplib
import base64
import sys

HOST = "127.0.0.1"
PATH = '/json/entry'
PORT = 5000

def show_help():
  print "Enter VERB URI Content"
  print "Examples:"
  print "  PUT /entry/<data>"
  print "  GET /get/json"
  print "  GET /get/xml"
  print "  GET /get/protobuf"
  print "  exit - leave the program\n"


HOST = raw_input('Enter Server IP address ')
if HOST == '':
    HOST = '127.0.0.1'

conn = httplib.HTTPConnection(HOST, 5000)

while True:

  s = raw_input('HTTP$ ')
  if s == '?':
    show_help()
  elif s == 'exit':
    sys.exit(0)
  elif s[0:3] == 'GET':
    verb = 'GET'
  elif s[0:3] == 'PUT':
    verb = 'PUT'
  elif s[0:4] == 'POST':
    verb = 'POST'
  elif s[0:6] == 'DELETE':
    verb = 'DELETE'

  else:
    print "Invalid VERB"
    continue

  s = s[len(verb) + 1:]
  if s.find(' ') > 0:
    uri = s[0:s.find(' ')]
  else:
    uri = s

  data = ''
  if s.find(' ') > 0:
    data = s[s.find(' ')+1:]

  if verb == 'POST':
    conn.request(verb, uri, data )
  else:
    conn.request(verb, uri)
  r1 = conn.getresponse()

  print "status " + str(r1.status) + ", reason " + str(r1.reason)
  data1 = r1.read()
  print "return data: " + str(data1)

conn.close()

