# protoc --python_out=. simple.proto
# creates file simple_pb2.py
import sys
import simple_pb2

foo = simple_pb2.Test1()
foo.n = raw_input("Enter name: ")
foo.a = int(raw_input("Enter age: "))

with open("simple.bin", "wb") as f:
  f.write(foo.SerializeToString())
  print "created 'simple.bin'\n"
