#!/usr/bin/python
import string 
import json
import sys
import base64
import simple_pb2

PORT = 5000

from flask import Flask, request

#=============================== APIs ====================================

#create the global objects
app = Flask(__name__)
name = "person"
age = 0

# ================== POST: /entry<data> ====================================
# change information kept for a person
# curl --data 'name=tessa&age=23' http://iot2e20:5000/entry
# -----------------------------------------------------------------------
@app.route("/entry", methods=['POST'])
def define_person():
    global name
    global age
    entry = request.data
    name = str(request.form['name'])
    age = int(str(request.form['age']))

    print "Entry received! Name: " + name + "Age: " + str(age)

    return "Entry received! Name: " + name + "Age: " + str(age)

# ================== GET: /get/json ====================================
# get json representation on person information
# curl http://iot2e20:5000/get/json
# -----------------------------------------------------------------------
@app.route("/get/json", methods=['GET'])
def get_json():
    rsp_json = '{"name":' + name + ',"age":' + str(age) + '}\n'
    return rsp_json, 200

# ================== GET: /get/xml ====================================
# get xml representation on person information
# curl http://iot2e20:5000/get/xml
# -----------------------------------------------------------------------
@app.route("/get/xml", methods=['GET'])
def get_xml():
    rsp_xml = '<name>' + name + '</name><age>' + str(age) + '</age>'
    return rsp_xml, 200

# ================== GET: /get/protobuf ====================================
# get protobuf representation on person information
# curl http://iot2e20:5000/get/protbuf
# -----------------------------------------------------------------------
@app.route("/get/protobuf", methods=['GET'])
def get_proto():
    myvar = simple_pb2.Test1()
    myvar.n = name
    myvar.a = age
    mystr = myvar.SerializeToString()
    rsp_proto = base64.b64encode(mystr)
    return rsp_proto, 200

#============================== Main =====================================

if __name__ == "__main__":
    print "Server"
    app.run(host= '0.0.0.0', debug =True, threaded=True, port=PORT)

