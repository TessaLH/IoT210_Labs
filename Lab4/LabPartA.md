[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Lab4 Part A - Cryptographics 

In this section of the lab you will learn

* Hash functions and when to use them
* Encryption algorithms and when to use them
* Public-key algorithms and when to use them

We'll use the Rapsberry Pi for exploring cryptography.

## Step 1 - Install pycrypto

Install pycrypt on your Rapsberry Pi

Either: 

```
pi$ sudo apt-get install python-crypto
```

Or do: 

```
pi$ sudo apt-get install python2.7-dev
pi$ sudo pip install pycrypto
```

Verify installation worked

```
pi$ python
>>> from Crypto.Hash import SHA256
>>> hash = SHA256.new('Hello World').hexdigest()
>>> print 'hash len(' + str(len(hash) / 2) + ') ' + hash
```

Try hashing other text.

## Step 3 - Hash Algorithms

This explores hashing by making a user/password file.

```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab4/src
pi$ iotsha256.py
```

Try entering a few different users and passwords by pressing Enter (no name)
to create new users.

Try logging into each account by typing username and password.

Try logging in with the wrong password.

Let's examine the file passwords.txt.

Easy: guess the password for user 'hello'.

Harder: See if you can determine the password for user "drew".

Hint: http://md5decrypt.net/en/Sha256/

Hard: See if you can determine the password for user "joe"


## Step 4 - Block Cipher (AES)

This lab uses MQTT to send/receive encrypted text.

Install paho-mqtt

```
pi$ pip install paho-mqtt
```

This uses a global built-in key.

```
pi$ python mqtt_subscribe_aes.py
```

In another SSH terminal window on your Raspberry PI, run the MQTT
publish application. This requires you to enter a key

```
pi$ python mqtt_publish_aes.py secretkey
```

Publish some lines of text. Notice the text is encoded on sending,
and decoded on receiving at the Subscriber.

Try changing your key on the subscriber (by editing the mqtt_subscribe_aes.py).

Make sure to change the key on the cmdline for the publisher.

Note: the AES key must either by 16 bytes (AES-128) or 32 bytes (AES-256)


## Step 5 - Public Key (RSA)

```
pi$ python iotpki.py The quick brown fox jumped over the lazy dog.
```

Modify the application to generate a 2048-bit key.


## Step 6 - Signing a Public Key

Signing a message can be useful to check the author of a message and make
sure we can trust its origin. Next is an example on how to sign a message.
The hash for this message is calculated first and then passed to the
sign() method of the RSA key.

Start by running python from SSH on the Rapsberry Pi.

**Signing** 

```
$pi python
>>> from Crypto.Hash import SHA256
>>> from Crypto.PublicKey import RSA
>>> from Crypto import Random
>>> random_generator = Random.new().read
>>> key = RSA.generate(1024, random_generator)
>>> text = 'my_signature'
>>> hash = SHA256.new(text).digest()
>>> signature = key.sign(hash, '')
>>> public_key = key.publickey()
```

**Verifying**

Knowing the public key, it is easy to verify a message. The plain text is sent
to the user along with the signature. The receiving side calculates the hash
value and then uses the public key verify() method to validate its origin.

```
>>> text = 'my_signature'
>>> hash = SHA256.new(text).digest()
>>> public_key.verify(hash, signature)
True

>>> text = 'not_my_signature'
>>> hash = SHA256.new(text).digest()
>>> public_key.verify(hash, signature)
False
```

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartA.md) Cryptographics  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartB.md) TLS and self-signed certs  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartC.md) WPA2 and Access Points  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab/homework.md) Build your own AP with secure autoboot API Server  


[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
