[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Lab7 Part B - Making an encrypted website using self-certs

In this lab, you'll learn about Nginx

Steps:

1) Update the system

`pi$ sudo apt update`

2) Install Nginx Web Server

```
pi$ sudo apt install nginx
pi$ sudo service nginx start
```

3) Verify Nginx is working http://<YOUR_IP>
   Verify that Nginx SSL is NOT working https://<YOUR_IP>

4) Generate OpenSSL Certificate

```bash
$ ./generate_key.sh
Generating a 2048 bit RSA private key
.........................................................................+++
.......+++
writing new private key to '{hostname}.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:Washington
Locality Name (eg, city) []:Bellevue
Organization Name (eg, company) [Internet Widgits Pty Ltd]:IOT 110
Organizational Unit Name (eg, section) []:IOT 110 (B) Spring
Common Name (e.g. server FQDN or YOUR name) []:Josh Welschmeyer
Email Address []:joshwelschmeyer@gmail.com
```

5) Create the folders to store generated keys

`$ sudo mkdir -p /etc/ssl/certs/iot /etc/ssl/certs/iot/private`

6) Copy the keys to the created folders

```bash
$ sudo cp {hostname}.crt /etc/ssl/certs/iot
$ sudo cp {hostname}.key /etc/ssl/certs/iot/private
```

7) Stop Nginx

```
pi$ sudo service nginx stop
```

8) Update the Nginx Configuration

Remove the currently enabled site configuration

```
pi$ sudo rm /etc/nginx/sites-enabled/default
```

Copy the new site configuration

Edit the iot-nginx.conf for your local hostname.

```
pi$ nano iot-nginx.conf
pi$ sudo cp iot-nginx.conf /etc/nginx/sites-enabled
```

9) Start your Flash application as a background process

```
pi$ pwd
/home/pi/Documents/Git/uw_iot_210_student/Lab7/src
pi$ python apserver.py &
```

10) Start Nginx

```
pi$ sudo service nginx start
pi$ hostname -I
  172.22.194.141
```

11) On your desktop, curl to your IP address (returned by hostname)

```
curl 172.22.194.141/api/v1?hello=world
```


[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartA.md) Cryptographics  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartB.md) TLS and self-signed certs  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartC.md) WPA2 and Access Points  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab/homework.md) Build your own AP with secure autoboot API Server  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
