[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)


# Lab7 Part C - Making your Rapsberry PI an Access Point

An access point is not like a host. It has it's own SSID and passphrase.

## Step 1 - Backup your Raspberry Pi 3 disk image

Currently, you use your Raspberry Pi as a Host (that is it joins a Wi-Fi
or other network). You'll probably want it back that way later.

Besides, it's always a good idea to back up your Raspberry Pi.

https://thepi.io/how-to-back-up-your-raspberry-pi/

This tutorial also teaches you how to restore your code image.


## Step 2 - Follow ThePi.io Steps to setup AP

Follow the steps at The Pi (a good site with Raspberry Pi tutorials):

**Important: pick a unique name for your SSID (network name).**

https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/

Remember what SSID (network name) and WPA2 password (see Step 5 in the above tutorial,
as you'll need it for Step 2 below.

Your Raspberry Pi should be in the list of Wi-Fi networks on your laptop
if configured properly.


## Step 2 - Alternative, if the above doesn't work for you, you can try:

As an alternative to thepi.io site, try going to follow the Ada Fruit Tutorial,
which is checked into the docs folder.


## Step 3 - Connect your laptop computer to your Raspberry Pi

After making your access point, connect your laptop computer to it.

Your laptop should now be on the Raspberry Pi's network. Check that
by looking at the address.

On Windows, in the cmdline (type Window key, then cmd) use ipconfig.

```
ipconfig
```

On Linux/Mac, use the terminal and ifconfig.

```
ifconfig | grep inet
```


## Lab Links

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartA.md) Cryptographics  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartB.md) TLS and self-signed certs  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab4/LabPartC.md) WPA2 and Access Points  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab/homework.md) Build your own AP with secure autoboot API Server  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
