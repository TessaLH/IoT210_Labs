[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Week 7 - IoT Security

## Lab Objectives

In this lab you'll learn:

* How to secure your IoT devices and transactions
* What are the different cryptographic algorithms and their uses
* How to set up the Rapsberry Pi as an Access Point with WPA2
* How TLS works and how to create self-signed certs for lab work
* How to create a secure web page

## Lab Links

[PART A](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab7/LabPartA.md) Cryptographics  
[PART B](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab7/LabPartB.md) TLS and self-signed certs  
[PART C](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab7/LabPartC.md) WPA2 and Access Points (AP)  
[Homework](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab/homework.md) Build your own AP with secure autoboot API Server  

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
