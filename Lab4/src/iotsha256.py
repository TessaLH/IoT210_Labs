#!/usr/bin/python

# =============================================================================
#        File : iotsha256.py
# Description : Demonstrate SHA256 Hash
#      Author : Drew Gislsason
#        Date : 5/11/2017
# =============================================================================
from Crypto.Hash import SHA256
import base64
import getpass
import sys
import string

FILENAME = "passwords.txt"

# if username matches, return password sha256
def iot_find_password_sha(uname):
  f = open(FILENAME,'r')
  sha = None
  for line in f:
    i = string.find(line, ',')
    if i > 0 and uname == line[0:i]:
      sha = line[i+1:-1]
  f.close()
  return sha

# create a user with hashed password
def iot_create_account():

  # don't allow creating the same name twice
  uname = raw_input("Enter username to create: ")
  if iot_find_password_sha(uname):
    print "Account already exists."
    return

  # get username and password in one line
  pwd   = getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  line  = uname + ',' + sha1 + '\n'

  # append it to the file
  f = open(FILENAME,'a')
  f.write(line)
  f.close()
  print "Account for " + uname + " created."

# login a user with hashed password
def iot_login_account(uname):

  # get sha from password
  pwd   = getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()

  # login if the shas match
  sha2  = iot_find_password_sha(uname)
  if sha2 == sha1:
    print "Welcome " + uname + "! Logged in."
  else:
    print "Login failed!!"

if __name__ == "__main__":
  print "iotsha256"
  while True:
    uname = raw_input("\nEnter name to login (leave empty for new user): ")
    if uname == 'exit':
      break
    elif uname == '':
      iot_create_account()
    else:
      iot_login_account(uname)
