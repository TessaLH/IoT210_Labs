#!/usr/bin/python
# =============================================================================
#        File : mqtt_publish_aes.py
# Description : publish data to an MQTT Broker encrypting with AES
#      Author : Drew Gislsason
#        Date : 5/11/2017
# =============================================================================
# pip install paho-mqtt
import sys
import paho.mqtt.client as mqtt
from Crypto.Cipher import AES
import base64

# makes string a multiple of blocksize n
def expand_data(s, n):

  if len(s) % n:
    s = s + (n - (len(s) % n)) * '~'
  return s

# key will either be 16 (AES-128) or 32 (AES-256) bytes in length
def aes_expand_key(key):
  global aesobj
  global blocksize

  if len(key) < 16:
    key = expand_data(key, 16)
    blocksize = 16
  elif len(key) > 16 and len(key) < 32:
    key = expand_data(key, 32)
    blocksize = 32
  else:
    key = key[0:32]
    blocksize = 32

  aesobj = AES.new(key, AES.MODE_ECB)
  return aesobj, blocksize

if __name__ == "__main__":

  # process command-line arguments
  if len(sys.argv) < 2:
    print "mqqt_publish key [topic]\n"
    exit(1)

  aesobj, blocksize = aes_expand_key(sys.argv[1])

  if len(sys.argv) >= 3:
    topic_name = sys.argv[2] 
  else:
    topic_name = "iot210"
  print "Publishing to topic " + topic_name

  client = mqtt.Client()
  client.connect("iot.eclipse.org", 1883, 60)
  client.loop_start()

  while True:
    s = raw_input("\nEnter a string to publish: ")
    s = expand_data(s, blocksize)
    encrypted_s = aesobj.encrypt(s)
    base64_s = base64.b64encode(encrypted_s)
    s = expand_data(base64_s, blocksize)
    print "Encrypted form: " + s
    client.publish(topic_name, s)
