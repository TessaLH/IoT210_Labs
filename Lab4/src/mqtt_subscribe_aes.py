#!/usr/bin/python
# =============================================================================
#        File : mqtt_subscribe_aes.py
# Description : Subscribe to data from an MQTT Broker using AES decrypt
#      Author : Drew Gislsason
#        Date : 5/11/2017
# =============================================================================
# pip install paho-mqtt
import sys
import paho.mqtt.client as mqtt
from Crypto.Cipher import AES
import base64

blocksize = 16            # must be 32 or 16
key = 'eecretkey~~~~~~~'  # must be 32 or 16 bytes

# makes string a multiple of blocksize n
def expand_data(s, n):

  if len(s) % n:
    s = s + (n - (len(s) % n)) * '~'
  return s

# strip off ~
def strip_data(s):
  i = len(s)
  while i:
    if s[i-1] != '~':
      break
    i -= 1
  s = s[0:i]
  return s

def aes_decode(s):
  global key

  # decode the data
  aesobj = AES.new(key, AES.MODE_ECB)
  base64_s = strip_data(s)
  encrypted_s = base64.b64decode(base64_s)
  s = aesobj.decrypt(encrypted_s)
  s = strip_data(s)

  return s

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
  print("Connected with result code " + str(rc))

  # Subscribing in on_connect() means that if we lose the connection and
  # reconnect then subscriptions will be renewed.
  print "\nSubscribing to topic " + topic_name
  client.subscribe(topic_name)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
  print "Encrypted form: " + msg.payload
  s = aes_decode(msg.payload)
  print(msg.topic + ": " + str(s))

if __name__ == "__main__":

  # process command-line arguments
  print "\nmqqt_subscribe [topic]\n\n"

  if len(sys.argv) >= 2:
    topic_name = sys.argv[1] 
  else:
    topic_name = "iot210"

  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.connect("iot.eclipse.org", 1883, 60)

  # Blocking call that processes network traffic, dispatches callbacks and
  # handles reconnecting.
  # Other loop*() functions are available that give a threaded interface and a
  # manual interface.
  client.loop_forever()
