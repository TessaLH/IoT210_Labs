[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Lab5 Part A - Dropbox API

In this section of the lab you will learn:

* How add the Dropbox API to a python program
* How to "salt" passwords so they can't be easily hacked

We'll use the Rapsberry Pi for exploring cryptography.

## Step 1 - Setup a Dropbox Account

Create a Dropbox account and login

1. On your desktop PC, go to https://www.dropbox.com/developers/apps/create
and choose "Dropbox API App" (not the Business API)
2. Choose "App Folder - Access to a single folder created specifically for your app."
3. Name the app "iot210winter", and Create the app.
4. On the iot210winter app Settings tab, choose Generate Access Token.
5. You'll need this access token (a long string of random ASCII characters),
   so leave page open or copy the the access token to someplace safe.

You can always get back app via https://www.dropbox.com/developers/apps

## Step 2 - Install dropbox on your Raspberry Pi

Install dropbox on your Rapsberry Pi. You'll need to clone the github
repository (the older v1 doesn't work anymore).

```
pi$ cd ~/Documents/Git
pi$ git clone git://github.com/dropbox/dropbox-sdk-python.git
pi$ cd dropbox-sdk-python
pi$ sudo python setup.py install
```

Verify installation worked.

```
pi$ python
>>> import dropbox
>>> dbx = dropbox.Dropbox("YOUR_ACCESS_TOKEN")
>>> dbx.users_get_current_account()
```

## Step 3 - Python Program To Try Out Dropbox API

This step uses the same login program we used during the security class,
but modified to use dropbox (now called dropbox_sha256.py) for file storage.

You'll need to add the token from step 1 into the program for it to work.

```
pi$ cd ~/Documents/Git/uw_iot_210_student
pi$ git pull
pi$ cd Lab5/src
pi$ nano dropbox_sha256.py
```

Then, run the program. It should behave like it did last week (able
to create new users and login as existing users).

On your PC web browser, Go to: https://www.dropbox.com/home/Apps/iot210winter.
Notice there is no "filename.txt".

Now run the program.

```
pi$ dropbox_sha256.py
```

Notice the file "passwords.txt" has been created in the Application Folder
at https://www.dropbox.com/home/Apps/iot210winter.

Try entering a few different users and passwords by pressing Enter (no name)
to create new users.

Try logging into each account by typing username and password.

Try logging in with the wrong password.

Let's examine the file passwords.txt on DropBox.

Let's examine the source code.

## Step 3 - Add Salt

Salt is the concept of not storing SHA256 string (like passwords) as the
users type them, but with some hidden algorithm modifying them to make the
strings less recognizable.

In `dropbox_sha256.py` Look for the keyword `SALT`, a variable that is currently
empty. Make the variable point to a string such as `$n3@_`.

Delete `passwords.txt` off both Dropbox and your local Lab5/src folder.

Now notice that the password for 'hello', which is 'world', can no longer be
recognized by a tool such as md5decrypt.

See http://md5decrypt.net/en/Sha256/

## Lab Links

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartA.md) DropBox API  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartB.md) Philips Hue API  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartC.md) IFTTT API  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/homework.md) Build an API that stores something in DropBox  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
