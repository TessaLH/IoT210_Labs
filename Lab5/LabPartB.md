[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Lab5 Part B - Philips Hue API

In this section of the lab you will learn:

* How work with the Philips Hue API
* How to use MQTT to remotely access an IoT device
* Concepts of UPnP
* Concepts of a Bridge

## Some Philips Hue and Lighting Links

[Get Hue IP Addr](https://www.meethue.com/api/nupnp)  
[Philips Hue Login](https://developers.meethue.com/user/login)  
[Philips Hue Getting Started](https://developers.meethue.com/documentation/getting-started)  
[Philips Hue Lights API](https://developers.meethue.com/documentation/lights-api)  
[Some Colors in HSL](http://www.december.com/html/spec/colorhsl.html)  
[Wikipedia Description of HSL](https://en.wikipedia.org/wiki/HSL_and_HSV)  
[Wikipedia Description of CIE](https://en.wikipedia.org/wiki/CIE_1931_color_space)  

Manual Control via Browser  {ipAddr}/debug/clip.html


## Step 1 - Setting Up A Philips Hue Device

Go to the Philips hue login (above) and sign up.

Then, if setting up your own philips Hue device, follow the Getting Started above.

In class, a Philips Hue device is already set up for your use.


## Step 2 - Determine address of Philips Hue Gateway

In your browser, go to: https://www.meethue.com/api/nupnp

This should tell you the IP address of the Philips Hue Bridge.

Enter this IP address into hue_put.py (see BRIDGE_IP). The example below uses 10.0.1.15.

```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab5/src
pi$ nano hue_put.py
  BRIDGE_IP   = '10.0.1.15'
```

## Step 3 - Try Out the hue_put.py program

The Philips Hue uses JSON (like many APIs do).

field            | range      | description
---------------- | ---------- | -----------
"on"             | true,false | colors won't change if off
"hue"            | 0-65535    | 0=red, 21845=green, 43690=blue
"sat"            | 0-254      | 0=white (no color saturation)
"bri"            | 1-254      | 1=dimmest, 254=full bright
"transitiontime" | 0-6000     | in .1 seconds


```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab5/src
pi$ hue_put.put '{"on": true}'
pi$ hue_put.put '{"on": false}'
pi$ hue_put.put '{"on": true, "hue":0, "sat":254, "bri":128}'
```

For those working remotely, there is no internet access to the Hue device.
Instead, we'll connect to the hue device via mqtt. A local subscriber (called
`hue_mqtt_commander.py`) will command the Philips Hue from the local network.

```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab5/src
pi$ python hue_mqtt_publish.py
```

Try publishing the strings `{"on":false}` and later `{"on":true}`. Note: when
publishing, do not include the ticks. It should be pure JSON. If you don't remember
how to write JSON, try the following links:

http://www.json.org  
https://jsonlint.com  

If you want to locally see what everyone is doing to the Philips Hue (instead of
watching the Zoom video), try using the `hue_mqtt_subscribe.py` program in a
separate Raspberry Pi window.

```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab5/src
pi$ python hue_mqtt_subscribe.py
```

## Step 4 - Explore Philips Developer's Portal

Register (it will send you an email), then use the emailed link to log in.

https://developers.meethue.com/user/login
https://developers.meethue.com/documentation/getting-started
https://developers.meethue.com/documentation/lights-api

Try using curl to get the state of all the lights. Substitute your own
userID if using your own Philips Hue.

```
$pi curl 10.0.1.15/api/SZgzJfdaZcDzjqvmSqob-KL0o1WrDDpUAMblSEBc/lights
```

Or view the file `light_status.json` in the lab.


## Lab Links

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartA.md) DropBox API  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartB.md) Philips Hue API  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartC.md) IFTTT API  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/homework.md) Build an API that stores something in DropBox  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
