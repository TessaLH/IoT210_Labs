[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Lab5 Part C - ifthisthenthat (IFTTT)

In this section of the lab you will learn:

* What IFTTT is
* How to use IFTTT
* How to develop for IFTTT
* How to integrate IFTTT into your Python programs

## Step 1 - Sign Up for IFTTT

https://ifttt.com

## Step 2 - Create an action using the IFTTT Website

On your PC, sign up for twitter if you don't already have an account at

https://twitter.com

Then go to IFTTT to Create New Applet (click on your username, new Applet).

https://ifttt.com

This should bring you to a page of if[this]thenthat.

Click on "This" to connect to a trigger. Connect to twitter (making a tweet
on a particular hashtag. I used `#iot210`.

You'll need to allow IFTTT access to your twitter account.

Then click on "That" to connect the action. In this case, send an SMS.
It will ask for your phone #, and will text a code to your phone.

Enter the code.

Now IFTTT will connect the trigger (tweeting) to the action (texting).

Unfortunately, IFTTT doesn't do this immediately. It says it will
happend within the hour.


## Step 3 - IFTTT in a Python Program

Go to https://ifttt.com/maker_webhooks

Click on the documentation button. This will generate a unique key
for you. You can test it by doing the following:

```
curl -X POST https://maker.ifttt.com/trigger/ping/with/key/{mykey}

curl -X POST https://maker.ifttt.com/trigger/{event}/with/key/{mykey}
curl -X POST -H "Content-Type: application/json" -d '{"value1":"1","value2":"2","value3":"2"}' https://maker.ifttt.com/trigger/{event}/with/key/{mykey}
```

On your raspberry Pi, you must add the key to the program `ifttt_ping.py`.

```
pi$ cd ~/Documents/Git/uw_iot_210_student/Lab5/src
pi$ nano ifttt_ping.py
pi$ python ifttt_ping.py
```

## Lab Links

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartA.md) DropBox API  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartB.md) Philips Hue API  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartC.md) IFTTT API  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/homework.md) Build an API that stores something in DropBox  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
