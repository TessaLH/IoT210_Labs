[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)

# Week 5 - APIs, including DropBox, PhilipsHue, IFTTT

## Lab Objectives

In this lab you'll learn:

* How the DropBox API works
* How to secure your connection to an API
* How to control an IoT Device - Philips Hue, from an API
* How to link MQTT to an IoT Device
* How If-This-Then-That Works
* General concepts about connecting IoT Devices to the Cloud

## Lab Links

[PART A](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartA.md) DropBox API  
[PART B](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartB.md) Philips Hue API  
[PART C](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/LabPartC.md) IFTTT API  
[Homework](https://gitlab.com/Gislason/uw_iot_210_student/tree/master/Lab5/homework.md) Build an API that stores something in DropBox  

[IOT STUDENT HOME](https://gitlab.com/Gislason/uw_iot_210_student/blob/master/README.md)
