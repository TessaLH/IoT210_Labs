/*=============================================================================
        File : udp_server.c
 Description : UDP server using sockets
      Author : Drew Gislsason
        Date : 2/22/2018
=============================================================================*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFLEN  1024
#define PORT    3333

void die(char *s)
{
    printf("Error: %s\n",s);
    exit(1);
}

int main(int argc, char *argv[])
{
    struct sockaddr_in  si_me, si_other;
    int                 sock, i;
    unsigned            port = PORT;
    socklen_t           slen=sizeof(si_other);
    char                buf[BUFLEN];

    printf("UDP Server in C for IoT 210\n");

    if(argc > 1)
        port = atoi(argv[1]);
    printf("Listening on port %u\n",port);

    if ((sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
        die("can't open socket");

    memset((char *) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(port);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sock, (void *)&si_me, sizeof(si_me))==-1)
        die("can't bind");

    for(;;)
    {
        if (recvfrom(sock, buf, BUFLEN, 0, (void *)&si_other, &slen)==-1)
            die("recvfrom failed");
        printf("Received packet from %s:%d\nData: %s\n\n", 
            inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buf);
    }

    close(sock);
    return 0;
}
