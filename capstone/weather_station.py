#!/usr/bin/python
# =============================================================================
#        File : weather_station.py
# Description : Modified from http_server.py  -- weather station acts a server
#               to store weather information (temp/humidity/pressure from pi,
#               and weather forcast from meteorologist) that clients can access
#      Author : Tessa Lombard-Henley
#        Date : 3/1/2017
# =============================================================================
import random
import string
import json
import sys
import time
from sense_hat import SenseHat

PORT = 5000

from flask import Flask, request

sense = SenseHat()

forecast = "\n\t\t--- FORECAST ---\n"
commentary = "\n"

week = ["Sunday\t", "Monday\t", "Tuesday\t", "Wednesday", "Thursday", "Friday\t", "Saturday"]
temp = [70, 70, 70, 70, 70, 70, 70]
daily_breakdown = ["sunny", "sunny", "sunny", "sunny", "sunny", "sunny", "sunny"]

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

@app.route("/my/api", methods=['GET', 'HEAD' 'PUT', 'POST', 'DELETE'])
def ApiV1Echo():

  rsp_data = "Echo:\nMethod: "

  if request.method == 'GET':
    rsp_data += "GET"
  elif request.method == 'HEAD':
    rsp_data += "HEAD"
  elif request.method == 'PUT':
    rsp_data += "PUT"
  elif request.method == 'POST':
    rsp_data += "POST"
  elif request.method == 'DELETE':
    rsp_data += "DELETE"

  rsp_data += "\nArgs: "
  for arg in request.args:
    rsp_data += str(arg) + ":" + request.args[arg] + " "

  rsp_data += "\nData:\n"
  rsp_data += request.get_data()
  rsp_data += "\n"

  return rsp_data, 200


# ================ API for reading weather report ================ #
@app.route("/weather/report", methods = ['GET'])
def read_report():
  global commentary
  report = "\n\n*-*-*-*-*-* SEATTLE WEATHER REPORT *-*-*-*-*-*\n"
  if request.method == 'GET':
	report += sensors()
	report += read_forecast()
	report += commentary

  return report, 200

# ============ forecast ===========================================#
@app.route("/forecast", methods = ['GET', 'POST', 'PUT', 'DELETE'])
def meteorologist_forecast():
  global week
  global commentary
  message = ""

  if request.method == 'POST':
    commentary += request.get_data() + "\n"
    message = "\nYour forecast commentary has posted correctly\n"

  elif request.method == 'GET':
    message =  "\nfetching forecast...\n"

  elif request.method == 'PUT':
    day = update_forecast(str(request.get_data()))
    message = "\nYour forecast has uploaded correctly for" + week[day] + "\n"

  elif request.method == 'DELETE':
    commentary = ""
    message = "\nYour forecast commentary has been deleted\n"

  return message + read_forecast() + commentary, 200

  # ============ TODAY: sensor readings =================================#
@app.route("/today", methods = ['GET'])
def sensorapi():
  if request.method == 'GET':
    return sensors(), 200

  return 400


def read_forecast():
  global forecast
  report = ""
  report += forecast
  i = 0
  for day in week:
    report += week[i] + "\t Temperature: " + str(temp[i]) + "F\t"
    report += daily_breakdown[i] + "\n"
    i += 1

  return report


def sensors():
  sens_str = "\n\t\t--- TODAY ---\n"
  t = round(sense.get_temperature(), 1)
  p = round(sense.get_pressure(), 1)
  h = round(sense.get_humidity(), 1)

  sens_str += "Temperature = %s C\nPressure = %s Millibars\nHumidity = %s %%rH\n" % (t,p,h)

  return sens_str


def update_forecast(str_in):
  week = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
  global temp
  global daily_breakdown
  i = 0
  d = 0
  splited = str_in.split()

  for day in week:
     if (splited[0] == day):
	temp[i] = splited[1]
        daily_breakdown[i] = splited[2]
	d = i
     i += 1
  return d

# ============================== Main ====================================

if __name__ == "__main__":

  print "Seattle Weather Station"

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)


